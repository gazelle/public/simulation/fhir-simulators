package net.ihe.gazelle.fhir.server.business;


import net.ihe.gazelle.evsapi.client.business.response.ValidationResult;
import net.ihe.gazelle.evsapi.client.business.response.ValidationStatus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidatorinterceptorTest {

    private ValidatorInterceptor validatorInterceptor;


    @Test
    public void isUrlValidTest() {
        validatorInterceptor = new ValidatorInterceptor("http://localhost", null);
        assertTrue(validatorInterceptor.isUrlValid("http://localhost/1.1.1.1"));
    }

    @Test
    public void isUrlNotValidTest() {
        validatorInterceptor = new ValidatorInterceptor("http://localhost", null);
        assertFalse(validatorInterceptor.isUrlValid(""));
        assertFalse(validatorInterceptor.isUrlValid(null));
    }

    @Test
    public void isValidateResultValidTest() {
        validatorInterceptor = new ValidatorInterceptor("http://localhost", null);
        assertTrue(validatorInterceptor.isValidationResultValid(new ValidationResult().setStatus(ValidationStatus.DONE_PASSED)));
    }

    @Test
    public void isValidateResultInvalidTest() {
        validatorInterceptor = new ValidatorInterceptor("http://localhost", null);
        assertFalse(validatorInterceptor.isValidationResultValid(new ValidationResult().setStatus(ValidationStatus.DONE_FAILED)));
        assertFalse(validatorInterceptor.isValidationResultValid(new ValidationResult().setStatus(ValidationStatus.DONE_UNDEFINED)));
        assertFalse(validatorInterceptor.isValidationResultValid(new ValidationResult().setStatus(null)));
        assertFalse(validatorInterceptor.isValidationResultValid(null));


    }


}
