package net.ihe.gazelle.fhir.server.business;

import net.ihe.gazelle.evsapi.client.business.ValidationService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ValidationConfigurationTest {

     public ValidationConfiguration validationConfiguration;



    @Test
    public void testGetEvsEndpoint() {
        validationConfiguration = new ValidationConfiguration()
                .setEvsEndpoint("http://localhost")
                .setValidationService(new ValidationService().setName("Test Service").setValidator("Test Validator"));
        String expectedEndpoint = "http://localhost";
        String actualEndpoint = validationConfiguration.getEvsEndpoint();
        assertEquals(expectedEndpoint, actualEndpoint);
    }

    @Test
    public void testGetValidationServiceName() {
        validationConfiguration = new ValidationConfiguration()
                .setEvsEndpoint("http://test-endpoint.com")
                .setValidationService(new ValidationService().setName("Test Service").setValidator("Test Validator"));
        String expectedServiceName = "Test Service";
        String actualServiceName = validationConfiguration.getValidationService().getName();
        assertEquals(expectedServiceName, actualServiceName);

    }
@Test
    public void testGetValidationServiceValidator() {
    validationConfiguration = new ValidationConfiguration()
            .setEvsEndpoint("http://test-endpoint.com")
            .setValidationService(new ValidationService().setName("Test Service").setValidator("Test Validator"));
        String expectedValidator = "Test Validator";
        String actualValidator = validationConfiguration.getValidationService().getValidator();
        assertEquals(expectedValidator, actualValidator);
    }
}