package net.ihe.gazelle.fhir.server.business;


import net.ihe.gazelle.evsapi.client.business.ValidationService;

public class ValidationConfiguration {
    private String evsEndpoint;
    private ValidationService validationService;

    public String getEvsEndpoint() {
        return evsEndpoint;
    }

    public ValidationConfiguration setEvsEndpoint(String evsEndpoint) {
        this.evsEndpoint = evsEndpoint;
        return this;
    }

    public ValidationService getValidationService() {
        return validationService;
    }

    public ValidationConfiguration setValidationService(ValidationService validationService) {
        this.validationService = validationService;
        return this;
    }

}
