package net.ihe.gazelle.fhir.server.technical;

import ca.uhn.fhir.interceptor.api.Hook;
import ca.uhn.fhir.interceptor.api.Pointcut;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.ihe.gazelle.evsapi.client.business.request.HandledObject;
import net.ihe.gazelle.evsapi.client.business.request.ValidationRequest;
import net.ihe.gazelle.fhir.server.business.ValidatorInterceptor;
import net.ihe.gazelle.fhir.server.business.ValidatorServiceMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class FhirValidatorInterceptor extends ValidatorInterceptor {

    private static final String FHIR_VALIDATION_URL = "fhir-validation-url";


    public FhirValidatorInterceptor(String evsEndpoint, ValidatorServiceMapper validatorSelector) {
        super(evsEndpoint, validatorSelector);
    }


    @Override
    @Hook(Pointcut.SERVER_INCOMING_REQUEST_POST_PROCESSED)
    public boolean incomingRequestPostProcessed(RequestDetails theRequestDetails, HttpServletRequest theRequest, HttpServletResponse theResponse) {
        String requestBody = null;
        try {
            requestBody = getContentFromRequest(theRequest);
        } catch (IOException e) {
            throw new InternalErrorException(e);
        }
        ValidationRequest validationRequest = buildValidationRequest(theRequestDetails, requestBody);
        validateRequest(theResponse, validationRequest, FHIR_VALIDATION_URL, "FHIR validation failed !");

        return true;


    }

    String getContentFromRequest(HttpServletRequest theRequest) throws IOException {
        InputStream inputStream = theRequest.getInputStream();
        String result = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));
        inputStream.close();
        return result;
    }

    ValidationRequest buildValidationRequest(RequestDetails theRequestDetails, String requestBody) {

        return new ValidationRequest().setValidationService(getValidatorSelector().getValidationService(theRequestDetails)
        ).addValidationItem(new HandledObject().setContent(requestBody.getBytes()));
    }


}
