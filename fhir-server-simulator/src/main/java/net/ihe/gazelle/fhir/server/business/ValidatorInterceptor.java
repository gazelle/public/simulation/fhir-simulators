package net.ihe.gazelle.fhir.server.business;

import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.interceptor.InterceptorAdapter;
import jakarta.servlet.http.HttpServletResponse;
import net.ihe.gazelle.evsapi.client.business.request.ValidationRequest;
import net.ihe.gazelle.evsapi.client.business.response.ValidationResult;
import net.ihe.gazelle.evsapi.client.business.response.ValidationStatus;
import net.ihe.gazelle.evsapi.client.technical.ValidationClientHttpImpl;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.OperationOutcome;


public class ValidatorInterceptor extends InterceptorAdapter {
    private final String evsEndpoint;
    private final ValidatorServiceMapper validatorSelector;
    private final ValidationClientHttpImpl validationClientHttp;


    public ValidatorInterceptor(String evsEndpoint, ValidatorServiceMapper validatorSelector) {
        this.evsEndpoint = evsEndpoint;
        this.validatorSelector = validatorSelector;
        this.validationClientHttp = new ValidationClientHttpImpl(evsEndpoint);
    }

    public String getEvsEndpoint() {
        return evsEndpoint;
    }

    public ValidatorServiceMapper getValidatorSelector() {
        return validatorSelector;
    }

    public boolean shouldNotValidate(ValidationRequest validationRequest) {
        return validationRequest.getValidationService() == null
                || validationRequest.getValidationService().getValidator() == null
                || validationRequest.getValidationService().getValidator().isEmpty();
    }

    public void validateRequest(HttpServletResponse theResponse, ValidationRequest validationRequest, String headerName, String errorMessage) {
        if (shouldNotValidate(validationRequest)) {
            theResponse.addHeader(headerName, "No validation required");
            return;
        }
        String url = sendRequestToEVS(validationRequest);
        if (!isUrlValid(url)) {
            throw new InternalErrorException("Unfetchable validation url !");
        }

        String oid = url.substring(url.lastIndexOf("/")+1);
        ValidationResult validationResult = getValidationResult(oid);

        if (!isValidationResultValid(validationResult)) {
            OperationOutcome oo = new OperationOutcome();
            OperationOutcome.OperationOutcomeIssueComponent issue = oo.addIssue();
            issue.setSeverity(OperationOutcome.IssueSeverity.ERROR);
            issue.setCode(OperationOutcome.IssueType.INVALID);
            issue.setDetails(new CodeableConcept().addCoding(new Coding().setCode(errorMessage + " : " + url)));
            throw new InvalidRequestException(errorMessage, oo);

        } else {
            theResponse.addHeader(headerName, url);
        }

    }

    public boolean isUrlValid(String url) {
        return url != null
                && !url.isEmpty()
                && url.contains(getEvsEndpoint())
                && url.matches("^.+/([0-2])((\\.0)|(\\.[1-9][0-9]*))*$");
    }

    public boolean isValidationResultValid(ValidationResult validationResult) {
        return validationResult != null && validationResult.getStatus() != null && !validationResult.getStatus().equals(ValidationStatus.DONE_FAILED) && !validationResult.getStatus().equals(ValidationStatus.DONE_UNDEFINED);
    }

    public String sendRequestToEVS(ValidationRequest validationRequest) {
        return validationClientHttp.validate(validationRequest);
    }

    public ValidationResult getValidationResult(String oid) {
        return validationClientHttp.getValidationByOid(oid);
    }
}
