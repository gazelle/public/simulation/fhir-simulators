package net.ihe.gazelle.fhir.server.technical;

import ca.uhn.fhir.interceptor.api.Hook;
import ca.uhn.fhir.interceptor.api.Pointcut;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.ihe.gazelle.evsapi.client.business.request.HandledObject;
import net.ihe.gazelle.evsapi.client.business.request.ValidationRequest;
import net.ihe.gazelle.fhir.server.business.ValidatorInterceptor;
import net.ihe.gazelle.fhir.server.business.ValidatorServiceMapper;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class HTTPValidatorInterceptor extends ValidatorInterceptor {

    private static final String VERSION = "version";

    private static final String TRACE_ID = "traceId";

    private static final String PARENT_ID = "parentId";

    private static final String TRACE_FLAGS = "traceFlags";

    private static final String HTTP_VALIDATION_URL = "http-validation-url";


    public HTTPValidatorInterceptor(String evsEndpoint, ValidatorServiceMapper validatorSelector) {
        super(evsEndpoint, validatorSelector);
    }


    @Override
    @Hook(Pointcut.SERVER_INCOMING_REQUEST_POST_PROCESSED)
    public boolean incomingRequestPostProcessed(RequestDetails theRequestDetails, HttpServletRequest theRequest, HttpServletResponse theResponse) {
        ValidationRequest validationRequest = buildValidationRequest(theRequestDetails, buildContentFromRequest(theRequest));

        validateRequest(theResponse, validationRequest, HTTP_VALIDATION_URL, "HTTP validation failed !");
        manageTraceparentINRequest(theRequest, theResponse);
        return true;


    }

    private void manageTraceparentINRequest(HttpServletRequest theServletRequest, HttpServletResponse theServletResponse) {
        String traceparent = theServletRequest.getHeader("traceparent");
        if (traceparent != null && !traceparent.isEmpty()) {
            Map<String, String> mappedTraceparent = parseTraceparent(traceparent);
            if (isTraceparentIsValid(mappedTraceparent)) {
                UUID uuid = UUID.randomUUID();
                // Convert the UUID to a string and replace the "-" characters
                String uuidString = uuid.toString().replace("-", "");
                // Return the first 16 characters of the UUID string
                String traceparentResponse = mappedTraceparent.get(VERSION) + "-" + mappedTraceparent.get(TRACE_ID) + "-" + uuidString.substring(0, 16) + "-" + mappedTraceparent.get(TRACE_FLAGS);
                theServletResponse.setHeader("traceparent", traceparentResponse);
            }

        }


    }


    private boolean isTraceparentIsValid(Map<String, String> mappedTraceparent) {
        return mappedTraceparent.get(VERSION).equals("00")
                && mappedTraceparent.get(TRACE_ID).length() == 32
                && !mappedTraceparent.get(TRACE_ID).equals("00000000000000000000000000000000")
                && mappedTraceparent.get(PARENT_ID).length() == 16
                && !mappedTraceparent.get(PARENT_ID).equals("0000000000000000")
                && mappedTraceparent.get(TRACE_FLAGS).length() == 2;
    }

    public Map<String, String> parseTraceparent(String traceparent) {
        Map<String, String> parsed = new HashMap<>();
        if (traceparent != null && traceparent.length() == 55) {
            parsed.put(VERSION, traceparent.substring(0, 2));
            parsed.put(TRACE_ID, traceparent.substring(3, 35));
            parsed.put(PARENT_ID, traceparent.substring(36, 52));
            parsed.put(TRACE_FLAGS, traceparent.substring(53, 55));
        }
        return parsed;
    }

    public String buildContentFromRequest(HttpServletRequest theRequest) {
        StringBuilder builder = new StringBuilder();

        // Get the HTTP method (GET, POST, PUT, DELETE, etc.)
        builder.append(theRequest.getMethod()).append(" ");

        // Get the request URL
        builder.append(theRequest.getRequestURL());

        // Get the query string if it exists
        if (theRequest.getQueryString() != null) {
            builder.append("?").append(theRequest.getQueryString());
        }

        // Get the protocol (HTTP/1.1, HTTP/2, etc.)
        builder.append(" ").append(theRequest.getProtocol()).append("\n");

        // Get all the headers and their values
        Enumeration<String> headerNames = theRequest.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            builder.append(headerName).append(": ").append(theRequest.getHeader(headerName)).append("\n");
        }

        return builder.toString();
    }

    ValidationRequest buildValidationRequest(RequestDetails theRequestDetails, String requestBody) {
        return new ValidationRequest().setValidationService(getValidatorSelector().getValidationService(theRequestDetails)
        ).addValidationItem(new HandledObject().setContent(requestBody.getBytes()));
    }


}
