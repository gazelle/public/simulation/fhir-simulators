package net.ihe.gazelle.fhir.server.business;

import ca.uhn.fhir.rest.api.server.RequestDetails;
import net.ihe.gazelle.evsapi.client.business.ValidationService;


public interface ValidatorServiceMapper {

    public ValidationService getValidationService(RequestDetails theRequestDetails);
}
